package eu.deyanix.managerbook;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ManagerBookConfig {
	private static final SimpleDateFormat SDF_FILE = new SimpleDateFormat("yyyyMMddHHmmss");
	private static final JSONParser JSON_PARSER = new JSONParser();
	private static Map<UUID, JSONObject> properties = new HashMap<UUID, JSONObject>();
	private static File file;
	private static JSONObject rootObject;
	
	public static void load(File f) throws IOException, ParseException {
		file = f.getAbsoluteFile();
		
		boolean create;
		if (!file.getParentFile().isDirectory()) {
			file.getParentFile().mkdirs();
		}
		if (!(create = file.isFile())) {
			file.createNewFile();
		}
		
		FileReader reader = null;
		try {
			reader = new FileReader(file);
			Object o = JSON_PARSER.parse(reader);
			reader.close();
			if (o instanceof JSONObject) {
				rootObject = (JSONObject) o;
			}
		} catch (ParseException e) {
			if (reader != null) {
				reader.close();
			}
			rootObject = new JSONObject();
			if (create) {
				Files.move(Paths.get(file.toURI()), Paths.get(new File(file.getParentFile(), "properties-" + SDF_FILE.format(new Date()) + ".json").toURI()), StandardCopyOption.ATOMIC_MOVE);
				file = f.getAbsoluteFile();
				save();
			}
		}
	}
	
	public static void save() throws IOException {
		// rootObject.putAll(properties);
		FileWriter writer = new FileWriter(file);
		writer.write(rootObject.toJSONString());
		writer.close();
	}
	
	@SuppressWarnings("unchecked")
	public static <T> JSONObject getPath(String... arr) {
		// if (!properties.containsKey(uuid))
		// properties.put(uuid, new JSONObject());
		
		JSONObject obj = rootObject;// = properties.get(uuid);
		for (int i = 0; i < arr.length - 1; i++) {
			String prop = arr[i];
			if (obj == null || !obj.containsKey(prop) && !(obj.get(prop) instanceof JSONObject)) {
				obj.put(prop, new JSONObject());
			}
			obj = (JSONObject) obj.get(prop);
			
		}
		return obj;
	}
	
	public static Object getProperty(String key, Object defaultValue) {
		String[] arr = key.split("\\.");
		Object jvalue = getPath(arr).get(arr[arr.length - 1]);
		if (jvalue == null) {
			setProperty(key, defaultValue);
			return defaultValue;
		}
		return jvalue;
	}
	
	@SuppressWarnings("unchecked")
	public static void setProperty(String key, Object value) {
		String[] arr = key.split("\\.");
		getPath(arr).put(arr[arr.length - 1], value);
	}
	
	@SuppressWarnings({"unchecked", "rawtypes"})
	public static void addProperty(String key, Object value) {
		String[] arr = key.split("\\.");
		String parentString = arr[arr.length - 1];
		JSONObject parent = getPath(arr);
		
		Object obj = parent.get(parentString);
		Collection coll;
		if (obj != null) {
			if (obj instanceof Collection) {
				coll = (Collection) obj;
			} else {
				coll = new JSONArray();
				coll.add(obj);
			}
		} else {
			coll = new JSONArray();
		}
		
		coll.add(value);
		parent.put(parentString, coll);
	}
	
	public static Map<UUID, JSONObject> getProperties() {
		return properties;
	}
}
