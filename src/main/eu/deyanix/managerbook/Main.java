package eu.deyanix.managerbook;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import eu.deyanix.managerbook.api.Book;
import eu.deyanix.managerbook.gui.ManagerBook;

public class Main {
	public final static String VERSION = "2.2";
	public static final File APPDATA_FOLDER;
	
	static {
		String appdata;
		if (System.getProperty("os.name").toUpperCase().contains("WIN")) {
			appdata = System.getenv("APPDATA");
		} else {
			appdata = "/" + System.getProperty("user.home");
		}
		APPDATA_FOLDER = new File(appdata + "/ManagerBook/");
		APPDATA_FOLDER.mkdirs();
		
	}
	
	public static List<Book> loadBooks() {
		List<Book> books = new ArrayList<Book>();
		File folder = new File(APPDATA_FOLDER, "books");
		folder.mkdirs();
		for (File f : Objects.requireNonNull(folder.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".xml");
			}
		}))) {
			
			try {
				Book b = Book.parse(f);
				if (b != null)
					books.add(b);
			} catch (Exception e) {	
				e.printStackTrace();
			}
		}
		return books;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("ManagerBook v" + VERSION + " © deyanix");
		
		ManagerBookConfig.load(new File(APPDATA_FOLDER, "properties.json"));

		ManagerBook managerBook = new ManagerBook(loadBooks());
		managerBook.setTitle("ManagerBook v" + VERSION);
		managerBook.addWindowListener(new WindowAdapter() {
			@Override
            public void windowClosing(WindowEvent e) {
				try {
					ManagerBookConfig.save();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
	}
	
	
}
