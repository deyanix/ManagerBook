package eu.deyanix.managerbook.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;

import eu.deyanix.managerbook.api.Book;
import eu.deyanix.managerbook.api.ChapterList;

public class SearcherPanel extends JPanel {
	private static final long serialVersionUID = 2986134321722778731L;
	private final ManagerBook mgr;
	
	private final JTextField searcher = new JTextField();
	private JTree view;
	private JScrollPane scrollView;
	private boolean letterCaseState = false;
	private final List<Object> expanded = new ArrayList<Object>();
	
	public SearcherPanel(ManagerBook mgr) {
		this.mgr = mgr;
		resetTree();
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		JPanel top = new JPanel();
		top.setLayout(new BoxLayout(top, BoxLayout.LINE_AXIS));
		
		searcher.setMaximumSize(new Dimension(Integer.MAX_VALUE, 20));
		searcher.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				reload();
			}
			
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				reload();
			}
			
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				reload();
			}
			
			private void reload() {
				if (searcher.getText().isEmpty()) {
					resetTree();
				} else {
					searchTree(searcherText());
				}
			}
		});
		
		JButton letterCase = new JButton("Aa");
		letterCase.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				letterCase.setText((letterCaseState = letterCase.getText().equals("Aa")) ? "aa" : "Aa");
				searchTree(searcherText());
				mgr.repaint();
			}
		});
		
		top.add(searcher);
		top.add(letterCase);
		add(top);
		add(scrollView);
	}
	
	private String searcherText(String text) {
		return letterCaseState ? text : text.toLowerCase();
	}
	
	private String searcherText() {
		return searcherText(searcher.getText());
	}
	
	private void reloadTree(Vector<Object> books) {
		JTree tree = new JTree(books);
		tree.setCellRenderer(new SearcherTreeCellRenderer());
		tree.addTreeExpansionListener(new TreeExpansionListener() {
			@Override
			public void treeExpanded(TreeExpansionEvent arg0) {
				expanded.add(((DefaultMutableTreeNode) arg0.getPath().getLastPathComponent()).getUserObject());
			}
			
			@Override
			public void treeCollapsed(TreeExpansionEvent arg0) {
				expanded.remove(((DefaultMutableTreeNode) arg0.getPath().getLastPathComponent()).getUserObject());
			}
		});
		
		if (scrollView == null) {
			scrollView = new JScrollPane(tree);
		} else {
			scrollView.setViewportView(tree);
		}
		
		if (view == null) {
			view = tree;
			return;
		}
		
		DefaultTreeModel model1 = (DefaultTreeModel) tree.getModel();
		DefaultMutableTreeNode root1 = (DefaultMutableTreeNode) model1.getRoot();
		DefaultMutableTreeNode next1 = root1.getNextNode();
		
		while (next1 != null) {
			if (expanded.contains(next1.getUserObject())) {
				tree.expandPath(new TreePath(next1.getPath()));
			}
			next1 = next1.getNextNode();
		}
		view = tree;
	}
	
	public void resetTree() {
		Vector<Object> books = new SearcherVector<Object>(null);
		for (Book book : mgr.books) {
			if (book.isEnable()) {
				putTree(books, book);
			}
		}
		reloadTree(books);
	}
	
	private void putTree(Vector<Object> vector, ChapterList parent) {
		if (parent.getChapters().size() <= 0) {
			vector.add(parent);
		} else {
			Vector<Object> chapters = new SearcherVector<Object>(parent);
			for (ChapterList chapter : parent.getChapters()) {
				putTree(chapters, chapter);
			}
			vector.add(chapters);
		}
	}
	
	public void searchTree(String search) {
		Vector<Object> books = new SearcherVector<Object>(null);
		for (Book book : mgr.books) {
			if (book.isEnable()) {
				putTreeSearch(books, book, searcherText());
			}
		}
		reloadTree(books);
	}
	
	private boolean putTreeSearch(Vector<Object> vector, ChapterList parent, String search) {
		boolean out = false;
		if (parent.getChapters().size() <= 0) {
			if (out = searcherText(parent.getTitle()).contains(search)) {
				vector.add(parent);
			}
		} else {
			Vector<Object> chapters = new SearcherVector<Object>(parent);
			out = searcherText(parent.getTitle()).contains(search);
			
			for (ChapterList chapter : parent.getChapters()) {
				if (searcherText(chapter.getTitle()).contains(search)) {
					out = true;
				}
				if (putTreeSearch(chapters, chapter, search)) {
					out = true;
				}
			}
			if (out) {
				vector.add(chapters);
			}
		}
		
		return out;
	}
	
	class SearcherTreeCellRenderer implements TreeCellRenderer {
		DefaultTreeCellRenderer defaulttcr = new DefaultTreeCellRenderer();
		
		public BufferedImage resize(BufferedImage originalImage, float scaled) {
			int width = (int) (originalImage.getWidth() * scaled), height = (int) (originalImage.getHeight() * scaled);
			
			BufferedImage scaledBI = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = scaledBI.createGraphics();
			
			g.drawImage(originalImage, 0, 0, width, height, null);
			g.dispose();
			return scaledBI;
		}
		
		@SuppressWarnings("rawtypes")
		@Override
		public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
			Object obj = node.getUserObject();
			ChapterList element = null;
			if (obj instanceof SearcherVector) {
				SearcherVector vector = (SearcherVector) node.getUserObject();
				element = vector.getParent();
			}
			
			JPanel com = new JPanel();
			com.setLayout(new BoxLayout(com, BoxLayout.LINE_AXIS));
			
			if (element instanceof Book) {
				Book book = (Book) element;
				BufferedImage img = resize(book.getImage(), 0.10f);
				JLabel picLabel = new JLabel(new ImageIcon(img));
				com.add(picLabel);
			}
			
			JLabel label = new JLabel(node.toString());
			com.setBorder(new EmptyBorder(0, 0, 0, 0));
			
			com.add(label);
			
			Color bgColor;
			if (selected) {
				bgColor = defaulttcr.getBackgroundSelectionColor();
			} else {
				bgColor = defaulttcr.getBackgroundNonSelectionColor();
			}
			
			com.setBackground(bgColor);
			com.setEnabled(tree.isEnabled());
			return com;
		}
	}
	
	class SearcherVector<E> extends Vector<E> {
		private static final long serialVersionUID = -1442145142514521L;
		private final ChapterList parent;
		
		public SearcherVector(ChapterList parent) {
			this.parent = parent;
		}
		
		public ChapterList getParent() {
			return parent;
		}
		
		@Override
		public String toString() {
			if (searcher.getText().isEmpty()) {
				return parent.toString();
			} else {
				return parent.getHighlightTitle(searcher.getText(), letterCaseState);// return
																						// parent.toString().toLowerCase().replace(searcher.getText().toLowerCase(),
																						// "<font color='blue'>" +
																						// searcher.getText().toLowerCase()
																						// + "</font>");
			}
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + getOuterType().hashCode();
			result = prime * result + (parent == null ? 0 : parent.hashCode());
			return result;
		}
		
		@Override
		public boolean equals(Object obj) {
			
			if (this == obj) {
				return true;
			}
			
			if (getClass() != obj.getClass()) {
				return false;
			}
			SearcherVector<?> other = (SearcherVector<?>) obj;
			if (!getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (parent == null) {
				if (other.parent != null) {
					return false;
				}
			} else if (!parent.equals(other.parent)) {
				return false;
			}
			return true;
		}
		
		private SearcherPanel getOuterType() {
			return SearcherPanel.this;
		}
	}
}
