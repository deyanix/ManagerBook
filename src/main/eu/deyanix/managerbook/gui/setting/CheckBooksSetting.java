package eu.deyanix.managerbook.gui.setting;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;

import eu.deyanix.managerbook.api.Book;
import eu.deyanix.managerbook.gui.ManagerBook;
import eu.deyanix.managerbook.util.ObjectTag;

public class CheckBooksSetting extends SettingPanel {
	private static final long serialVersionUID = -5322834439493881739L;
	private final JList<ObjectTag<Book, Boolean>> books;
	
	@SuppressWarnings("unchecked")
	public CheckBooksSetting(ManagerBook mgr) {
		super(mgr, "Check books");
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		ObjectTag<Book, Boolean> list[] = ObjectTag.convertList(mgr.books).toArray(new ObjectTag[0]);
		for (ObjectTag<Book, Boolean> tag : list) {
			tag.setTag(tag.getObject().isEnable());
		}
		
		books = new JList<ObjectTag<Book, Boolean>>(list);
		books.setCellRenderer(new ManagerBookListRenderer());
		books.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				check(e);
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				check(e);
			}
			
			@Override
			public void mouseClicked(MouseEvent event) {
				JList<ObjectTag<Book, Boolean>> list = (JList<ObjectTag<Book, Boolean>>) event.getSource();
				
				int index = list.locationToIndex(event.getPoint());
				ListModel<ObjectTag<Book, Boolean>> model = list.getModel();
				boolean edited = false;
				for (int i = 0; i < model.getSize(); i++) {
					ObjectTag<Book, Boolean> item = model.getElementAt(i);
					if (i == index) {
						item.setTag(!item.getTag());
					}
					if (item.getTag() != item.getObject().isEnable()) {
						edited = true;
						break;
					}
				}
				setEdited(edited);
				
				list.repaint(list.getCellBounds(index, index));
			}
			
			public void check(MouseEvent e) {
				JList<ObjectTag<Book, Boolean>> list = (JList<ObjectTag<Book, Boolean>>) e.getSource();
				int index = list.locationToIndex(e.getPoint());
				
				ListModel<ObjectTag<Book, Boolean>> model = list.getModel();
				ObjectTag<Book, Boolean> obj = model.getElementAt(index);
				Object objMenu = obj.getObjectTag("popupMenu");
				if (objMenu != null && e.isPopupTrigger()) {
					((JPopupMenu) objMenu).show(list, e.getX(), e.getY());
				}
			}
		});
		add(new JScrollPane(books));
	}
	
	@Override
	public void onAction(int action) {
		if ((action & 0b1) > 0) {
			ListModel<ObjectTag<Book, Boolean>> modelBooks = books.getModel();
			for (int i = 0; i < modelBooks.getSize(); i++) {
				ObjectTag<Book, Boolean> object = modelBooks.getElementAt(i);
				Book b = object.getObject();
				
				b.setEnable(object.getTag());
				b.setProperty("enabled", object.getTag());
			}
			getManagerBook().searcherPanel.resetTree();
		}
	}
	
	class ManagerBookListRenderer extends JCheckBox implements ListCellRenderer<ObjectTag<Book, Boolean>> {
		private static final long serialVersionUID = 5088063343981423386L;
		
		@Override
		public Component getListCellRendererComponent(JList<? extends ObjectTag<Book, Boolean>> list, ObjectTag<Book, Boolean> value, int index, boolean isSelected, boolean cellHasFocus) {
			setEnabled(list.isEnabled());
			setSelected(value.getTag());
			setFont(list.getFont());
			setBackground(list.getBackground());
			setForeground(list.getForeground());
			setText(value.getObject().getTitle());
			
			if (value.getObjectTag("popupMenu") == null) {
				JPopupMenu popupMenu = new JPopupMenu();
				JMenuItem item = popupMenu.add("Information about book...");
				item.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent a) {
						Book book = value.getObject();
						JDialog dialog = new JDialog(getManagerBook());
						dialog.setSize(450, 550);
						dialog.setResizable(false);
						
						JPanel content = new JPanel();
						content.setLayout(new FlowLayout());
						content.add(new JLabel(new ImageIcon(book.getImage())));
						content.add(new JLabel("<html>Author: <b>" + book.getAuthor() + "</b></html>"));
						JLabel link = new JLabel("<html><b><a href='" + book.getLink().toString() + "'>Link to Helion</a></b></html>");
						link.setCursor(new Cursor(Cursor.HAND_CURSOR));
						link.addMouseListener(new MouseAdapter() {
							@Override
							public void mouseClicked(MouseEvent e) {
								try {
									Desktop.getDesktop().browse(book.getLink().toURI());
								} catch (IOException | URISyntaxException e1) {
									e1.printStackTrace();
								}
							}
						});
						content.add(link);
						content.setAlignmentX(Component.CENTER_ALIGNMENT);
						dialog.setContentPane(content);
						dialog.setVisible(true);
					}
				});
				value.setObjectTag("popupMenu", popupMenu);// setComponentPopupMenu(popupMenu);
			}
			return this;
		}
	}
}
