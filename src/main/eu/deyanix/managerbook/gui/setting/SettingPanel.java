package eu.deyanix.managerbook.gui.setting;

import javax.swing.JPanel;

import eu.deyanix.managerbook.gui.ManagerBook;

public class SettingPanel extends JPanel {
	private static final long serialVersionUID = 8574691010458740087L;
	private final ManagerBook managerBook;
	private final String title;
	private final String description;
	private boolean edited = false;
	
	protected SettingPanel(ManagerBook mgr, String title) {
		this(mgr, title, null);
	}
	
	protected SettingPanel(ManagerBook mgr, String title, String description) {
		this.title = title;
		this.description = description;
		this.managerBook = mgr;
	}
	
	public void onAction(int action) {	
	}
	
	protected void setEdited(boolean edited) {
		this.edited = edited;
		managerBook.settings.refreshEdit();
	}
	
	public boolean isEdited() {	
		return edited;
	}
	
	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public ManagerBook getManagerBook() {
		return managerBook;
	}
}
