package eu.deyanix.managerbook.gui;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import eu.deyanix.managerbook.Main;
import eu.deyanix.managerbook.api.Book;
import eu.deyanix.managerbook.gui.dialog.SettingsDialog;
import eu.deyanix.managerbook.gui.setting.CheckBooksSetting;

public class ManagerBook extends JFrame {
	private static final long serialVersionUID = 1000599950156680748L;
	public final SearcherPanel searcherPanel;
	public List<Book> books;
	public SettingsDialog settings;
	
	public ManagerBook(Book book) {
		this(new Book[]{book});
	}
	
	public ManagerBook(Book... book) {
		this(Arrays.asList(book));
	}
	
	public ManagerBook(List<Book> books) {
		this.books = books;
		
		// Settings
		SettingsDialog.registerSetting(new CheckBooksSetting(this));
		
		// MenuBar
		JMenuBar bar = new JMenuBar();
		
		// Menu - Setting
		JMenu setting = new JMenu("File");
		
		/*
		 * //Item - Manager books JMenuItem mgrBook = new JMenuItem("Check books..."); mgrBook.addActionListener(new
		 * ActionListener() {
		 * @Override public void actionPerformed(ActionEvent arg0) { ManagerBookDialog dialog = new
		 * ManagerBookDialog(ManagerBook.this); dialog.setVisible(true); } }); setting.add(mgrBook);
		 */
		
		// Item - Open folder
		JMenuItem openFolder = new JMenuItem("Open ManagerBook folder");
		openFolder.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					Desktop.getDesktop().open(Main.APPDATA_FOLDER);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		setting.add(openFolder);
		
		// Item - Refresh
		JMenuItem refresh = new JMenuItem("Refresh");
		refresh.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ManagerBook.this.books = Main.loadBooks();
				searcherPanel.resetTree();
			}
		});
		setting.add(refresh);
		
		// Item - Settings
		JMenuItem settings = new JMenuItem("Settings");
		settings.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ManagerBook.this.settings = new SettingsDialog(ManagerBook.this);
				ManagerBook.this.settings.setVisible(true);
			}
		});
		setting.add(settings);
		
		bar.add(setting);
		setJMenuBar(bar);
		
		add(searcherPanel = new SearcherPanel(this));
		setSize(800, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
}
