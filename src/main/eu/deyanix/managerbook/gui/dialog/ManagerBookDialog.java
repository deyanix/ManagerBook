package eu.deyanix.managerbook.gui.dialog;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;

import eu.deyanix.managerbook.api.Book;
import eu.deyanix.managerbook.gui.ManagerBook;

public class ManagerBookDialog extends JDialog {
	private static final long serialVersionUID = -5322834439493881739L;
	private final ManagerBook mgr;
	private final JPanel content;
	private final JList<Book> books;
	// private JScrollPane scrollPane;
	
	public ManagerBookDialog(ManagerBook mgr) {
		super(mgr, "Manager books...");
		this.mgr = mgr;
		
		setSize(600, 400);
		setModal(true);
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent we) {
				ManagerBookDialog.this.mgr.searcherPanel.resetTree();
				ManagerBookDialog.this.mgr.repaint();
			}
		});
		content = new JPanel();
		content.setLayout(new BoxLayout(content, BoxLayout.PAGE_AXIS));
		
		books = new JList<Book>(mgr.books.toArray(new Book[0]));
		books.setCellRenderer(new ManagerBookListRenderer());
		books.addMouseListener(new MouseAdapter() {
			@Override
			@SuppressWarnings("unchecked")
			public void mouseClicked(MouseEvent event) {
				JList<Book> list = (JList<Book>) event.getSource();
				
				int index = list.locationToIndex(event.getPoint());
				Book item = list.getModel().getElementAt(index);
				item.setEnable(!item.isEnable());
				item.setProperty("enabled", item.isEnable());
				
				list.repaint(list.getCellBounds(index, index));
			}
		});
		content.add(new JScrollPane(books));
		setContentPane(content);
	}
	
	class ManagerBookListRenderer extends JCheckBox implements ListCellRenderer<Book> {
		private static final long serialVersionUID = 5088063343981423386L;
		
		@Override
		public Component getListCellRendererComponent(JList<? extends Book> list, Book value, int index, boolean isSelected, boolean cellHasFocus) {
			setEnabled(list.isEnabled());
			setSelected(value.isEnable());
			setFont(list.getFont());
			setBackground(list.getBackground());
			setForeground(list.getForeground());
			setText(value.toString());
			return this;
		}
	}
}
