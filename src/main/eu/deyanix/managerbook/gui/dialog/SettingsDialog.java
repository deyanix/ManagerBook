package eu.deyanix.managerbook.gui.dialog;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import eu.deyanix.managerbook.gui.ManagerBook;
import eu.deyanix.managerbook.gui.setting.SettingPanel;

public class SettingsDialog extends JDialog {
	private static final long serialVersionUID = -8907853716537314406L;
	private static final List<SettingPanel> settingPanels = new ArrayList<SettingPanel>();
	
	private static final int ACTION_SAVE = 0b11;
	private static final int ACTION_CANCEL = 0b10;
	private static final int ACTION_ACCEPT = 0b01;
	
	
	public static void registerSetting(SettingPanel panel) {
		settingPanels.add(panel);
	}
	
	private ManagerBook mgr;
	private JPanel mainPanel;
	private JTabbedPane tabbedPane;
	private JButton save, accept, cancel;
	
	
	public SettingsDialog(ManagerBook mgr) {
		super(mgr, "Settings");
		this.mgr = mgr;
		
		
		setSize(600, 400);
		setResizable(false);
		
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		tabbedPane = new JTabbedPane();
		initTabs();
		
		JPanel groupButtons = new JPanel();
		groupButtons.setLayout(new BoxLayout(groupButtons, BoxLayout.X_AXIS));
		
		save = new JButton("Save");
		accept = new JButton("Accept");
		cancel = new JButton("Cancel");
		
		save.addActionListener((a) -> {
			actionTabs(ACTION_SAVE);
		});
		groupButtons.add(save);
		
		accept.setEnabled(false);
		accept.addActionListener((a) -> {
			actionTabs(ACTION_ACCEPT);
		});
		groupButtons.add(accept);
		
		cancel.addActionListener((a) -> {
			actionTabs(ACTION_CANCEL);
		});
		groupButtons.add(cancel);
		
		mainPanel.add(tabbedPane, BorderLayout.NORTH);
		mainPanel.add(Box.createRigidArea(new Dimension(5,0)));
		mainPanel.add(groupButtons, BorderLayout.EAST);
		setContentPane(mainPanel);
		pack();
	}
	
	public void refreshEdit() {
		boolean edited = false;
		for (SettingPanel panel : settingPanels) {
			if (panel.isEdited()) {
				edited = true;
				break;
			}
		}
		accept.setEnabled(edited);
	}
	
	protected void actionTabs(int action) {
		for (SettingPanel panel : settingPanels)
			panel.onAction(action);
		if ((action & 0b10) > 0)
			this.setVisible(false);
	}
	
	protected void initTabs() {
		for (SettingPanel panel : settingPanels) {
			tabbedPane.addTab(panel.getTitle(), null, panel, panel.getDescription());
		}
	}

	public ManagerBook getManagerBook() {
		return mgr;
	}

}
