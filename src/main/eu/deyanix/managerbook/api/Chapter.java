package eu.deyanix.managerbook.api;

public class Chapter extends AbstractChapterList {
	private static final ChapterType STANDARD_TYPE = new ChapterType("Rozdział %number%.");
	
	private final Book book;
	private final ChapterType type;
	
	public Chapter(Book book, ChapterList parent, String title, int page) {
		this(book, parent, title, page, -1);
	}
	
	public Chapter(Book book, ChapterList parent, String title, int page, int number) {
		this(book, parent, title, page, number, null);
	}
	
	public Chapter(Book book, ChapterList parent, String title, int page, int number, ChapterType type) {
		super(title, page, parent, number);
		this.book = book;
		this.type = type != null ? type : STANDARD_TYPE;
	}
	
	@Override
	public ChapterList createChapter(String title, int page) {
		Chapter child = new Chapter(book, this, title, page);
		addChapter(child);
		return child;
	}
	
	@Override
	public ChapterList createChapter(String title, int number, int page) {
		Chapter child = new Chapter(book, this, title, number, page);
		addChapter(child);
		return child;
	}

	@Override
	public String toString() {
		if (getNumber() > 0)
			return type.glueHTML(getTitle(), getNumber(), getPage());
		else
			return type.gluePage(getTitle(), getPage());
	}

	@Override
	public String getHighlightTitle(String highlight, boolean letterCase) {
		StringBuilder
			builder = new StringBuilder(getTitle());
		
		int find = 0;
		while ((find = (!letterCase ? builder.toString().toLowerCase() : builder.toString()).indexOf((letterCase ? highlight : highlight.toLowerCase()), find)) >= 0) {
			String in = builder.toString().substring(find, find+highlight.length());
			String out = "<b><font color='#5cff47'>" + in + "</font></b>";
			
			builder.replace(find, find+in.length(), out);
			find += out.length();
		}

		if (getNumber() > 0)
			return type.glueHTML(builder.toString(), getNumber(), getPage());
		else
			return type.gluePage(builder.toString(), getPage());
	}
}
