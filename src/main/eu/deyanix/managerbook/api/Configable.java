package eu.deyanix.managerbook.api;

public interface Configable {
	public void setProperty(String key, Object value);
	public void addProperty(String key, Object value);
	public Object getProperty(String key, Object defaultValue);
}
