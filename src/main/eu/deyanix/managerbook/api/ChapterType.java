package eu.deyanix.managerbook.api;

public class ChapterType {
	interface NumericType {
		public String convertNumber(int number);
	}
	
	class ArabicNumericType implements NumericType {
		public String convertNumber(int number) {
			return String.valueOf(number);
		}
	}
	
	class RomanNumericType implements NumericType {
		public String convertNumber(int number) {
			int mInt = number;
			String[] rnChars = { "M",  "CM", "D", "C",  "XC", "L",  "X", "IX", "V", "I" };
		    int[] rnVals = { 1000, 900, 500, 100, 90, 50, 10, 9, 5, 1 };
		    String retVal = "";

		    for (int i = 0; i < rnVals.length; i++) {
		        int numberInPlace = mInt / rnVals[i];
		        if (numberInPlace == 0) continue;
		        retVal += numberInPlace == 4 && i > 0? rnChars[i] + rnChars[i - 1]:
		            new String(new char[numberInPlace]).replace("\0",rnChars[i]);
		        mInt = mInt % rnVals[i];
		    }
		    return retVal;
		}
	}
	
	private final String type;
	private final boolean upper;
	private NumericType numericType;
	
	public ChapterType(String type) {
		this(type, false);
	}
	
	public ChapterType(String type, boolean upper) {
		this(type, upper, "arabic");
	}
	
	public ChapterType(String type, boolean upper, String numericType) {
		this.type = type;
		this.upper = upper;
		if (numericType == null)
			numericType = "";
		switch (numericType) {
			case "roman":
				this.numericType = new RomanNumericType();
				break;
			case "arabic":
			default:
				this.numericType = new ArabicNumericType();
				break;
		}
	}
	
	private String text(String title) {
		return upper ? title.toUpperCase() : title;
	}
	
	public String glueNumber(String title, int number) {
		return type.replace("%number%", numericType.convertNumber(number)) + " " + text(title);
	}
	
	public String gluePage(String title, int page) {
		return "<html>" + text(title) + " <font color='green'>" + page + "</font></html>";
	}
	
	public String glueHTML(String title, int number, int page) {
		return "<html>" + "<font color='red'>" + type.replace("%number%", numericType.convertNumber(number)) + " " + " </font> " + text(title) + " <font color='green'>" + page + "</font></html>";
	}
}