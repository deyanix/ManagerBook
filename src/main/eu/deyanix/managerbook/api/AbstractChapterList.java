package eu.deyanix.managerbook.api;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public abstract class AbstractChapterList implements ChapterList {
	private boolean enable = true;
	private final int page, number;
	private final String title;
	private final ChapterList parent;
	private final List<ChapterList> chapters = new ArrayList<ChapterList>();
	private final UUID uuid;
	
	public AbstractChapterList(String title) {
		this(title, -1);
	}
	
	public AbstractChapterList(String title, int page) {
		this(title, page, null);
	}
	
	public AbstractChapterList(String title, int page, ChapterList parent) {
		this(title, page, parent, -1);
	}
	
	public AbstractChapterList(String title, int page, ChapterList parent, int number) {
		this(title, page, parent, number, null);
	}
	
	public AbstractChapterList(String title, int page, ChapterList parent, int number, UUID uuid) {
		this.title = title;
		this.page = page;
		this.parent = parent;
		this.number = number;
		this.uuid = uuid != null ? uuid : UUID.randomUUID();
	}
	
	public abstract ChapterList createChapter(String title, int page);
	public abstract ChapterList createChapter(String title, int number, int page);
	public abstract String getHighlightTitle(String highlight, boolean letterCase);
	
	public boolean isEnable() {
		return enable;
	}
	
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	
	public void addChapter(ChapterList chapter) {
		chapters.add(chapter);
	}
	
	public void removeChapter(ChapterList chapter) {
		chapters.remove(chapter);
	}
	
	public List<ChapterList> getChapters() {
		return new ArrayList<ChapterList>(chapters);
	}
	
	public ChapterList getParent() {
		return parent;
	}
	
	public int getPage() {
		return page;
	}

	public int getNumber() {
		return number;
	}

	public String getTitle() {
		return title;
	}
	
	public UUID getID() {
		return uuid;
	}
}
