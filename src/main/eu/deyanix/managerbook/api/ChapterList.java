package eu.deyanix.managerbook.api;

import java.util.List;

public interface ChapterList {
	public ChapterList createChapter(String title, int page);
	public ChapterList createChapter(String title, int number, int page);
	public ChapterList getParent();
	public void addChapter(ChapterList chapter);
	public void removeChapter(ChapterList chapter);
	public List<ChapterList> getChapters();
	public String getTitle();
	public String getHighlightTitle(String highlight, boolean letterCase);
	public boolean isEnable();
	public void setEnable(boolean enable);
}
