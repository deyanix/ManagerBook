package eu.deyanix.managerbook.api;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import eu.deyanix.managerbook.Main;
import eu.deyanix.managerbook.ManagerBookConfig;

public class Book extends AbstractChapterList implements Configable {
	private static final File FOLDER_IMAGE_CACHE = new File(Main.APPDATA_FOLDER, "images");
	private static final Map<UUID, Book> BOOKS = new HashMap<UUID, Book>();
	
	static {
		FOLDER_IMAGE_CACHE.mkdirs();
	}
	
	public static Book parse(UUID uuid) {
		return BOOKS.get(uuid);
	}
	
	public static Book parse(File file) throws Exception {
		return parse(new FileInputStream(file));
	}
	
	public static Book parse(InputStream in) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(in);
		
		Element elementBook = document.getDocumentElement();
		String author = null, title = null, link = null;
		UUID uuid = null;
		Element content = null;
		
		Map<String, ChapterType> chapterTypeList = new HashMap<String, ChapterType>();
		ChapterType defaultChapterType = null;
		
		NodeList list = elementBook.getChildNodes();
		for (int i = 0; i < list.getLength(); i++) {
			Node n = list.item(i);
			if (n instanceof Element) {
				Element e = (Element) n;
				switch (e.getTagName().toLowerCase()) {
					case "author":
						author = e.getTextContent();
						break;
					case "link":
						link = e.getTextContent();
						break;
					case "title":
						title = e.getTextContent();
						break;
					case "uuid":
						uuid = UUID.fromString(e.getTextContent());
						break;
					case "content":
						content = e;
						break;
					case "chaptertypelist":
						NodeList list1 = e.getChildNodes();
						for (int j = 0; j < list1.getLength(); j++) {
							Node n1 = list1.item(j);
							if (n1 instanceof Element) {
								Element e1 = (Element) n1;
								if (e1.getTagName().equalsIgnoreCase("chapterType")) {
									boolean upper = false, defaultType = false;
									String type = null, numeric = null, id = null;
									
									if (e1.hasAttribute("upper")) 
										upper = Boolean.parseBoolean(e1.getAttribute("upper"));
									if (e1.hasAttribute("numeric"))
										numeric = e1.getAttribute("numeric");
									if (e1.hasAttribute("default"))
										defaultType = Boolean.parseBoolean(e1.getAttribute("default"));
									if (e1.hasAttribute("id"))
										id = e1.getAttribute("id");
									type = e1.getTextContent();
									
									ChapterType chapterType = new ChapterType(type, upper, numeric);
									
									if (id != null)
										chapterTypeList.put(id, chapterType);
									if (defaultType)
										defaultChapterType = chapterType;
								}
							}
						}
						break;
				}
			}
		}
		
		
		//if (elementBook.hasAttribute("title"))
		//	title = elementBook.getAttribute("title");
		//if (elementBook.hasAttribute("author"))
		//	author = elementBook.getAttribute("author");
		try {
			Book book = new Book(title, author, uuid, defaultChapterType, new URL(link));
			book.setEnable(Boolean.valueOf(String.valueOf(book.getProperty("enabled", true))));

			String serverId = book.getServerID();
			if (serverId != null) {
				File f = new File(FOLDER_IMAGE_CACHE, serverId + ".jpg");
				if (f.isFile()) {
					book.setImage(ImageIO.read(f));
				}
			}

			putToBook(book, book, content, chapterTypeList);

			return book;
		} catch (Exception e) {
			return null;
		}
	}
	
	private static void putToBook(Book book, ChapterList chapter, Element in, Map<String, ChapterType> chapterTypeList) {
		NodeList list = in.getChildNodes();
		for (int i = 0; i < list.getLength(); i++) {
			if (list.item(i).getNodeType() == 1) {
				Element element = (Element) list.item(i);
				
				if (element.getTagName().equalsIgnoreCase("chapter")) {
					String title = null;
					int page = 0, number = -1;
					ChapterType type = null;
					
					if (element.hasAttribute("page"))
						page = Integer.parseInt(element.getAttribute("page"));
					
					if (element.hasAttribute("number"))
						number = Integer.parseInt(element.getAttribute("number"));
					else
						number = -1;
					
					if (element.hasAttribute("type")) 
						type = chapterTypeList.get(element.getAttribute("type"));
					if (type == null)
						type = book.getDefaultChapterType();
					
					Element chapterList = null;
					for (int j = 0; j < element.getChildNodes().getLength(); j++) {
						if (element.getChildNodes().item(j).getNodeType() == 1) {
							Element abstractTitle = (Element) element.getChildNodes().item(j);
							if (abstractTitle.getTagName().equalsIgnoreCase("title"))
								title = abstractTitle.getTextContent();
							else if (abstractTitle.getTagName().equalsIgnoreCase("chapterlist"))
								chapterList = abstractTitle;
						}
					}
					
					
					Chapter ch = new Chapter(book, chapter, title, page, number, type);

					chapter.addChapter(ch);
					if (chapterList != null)
						putToBook(book, ch, chapterList, chapterTypeList);
				}
			}
		}
	}
	
	private final ChapterType defaultType;
	private final String author; 
	private final URL link;
	private BufferedImage cacheImage;
	
	public Book(String title) {
		this(title, null);
	}
	
	public Book(String title, String author) {
		this(title, author, UUID.randomUUID());
	}
	
	public Book(String title, String author, UUID uuid) {
		this(title, author, uuid, null);
	}
	
	public Book(String title, String author, UUID uuid, ChapterType defaultType) {
		this(title, author, uuid, defaultType, null);
	}
	
	public Book(String title, String author, UUID uuid, ChapterType defaultType, URL link) {
		super(title, -1, null, -1, uuid);
		BOOKS.put(uuid, this);
		this.author = author;
		this.defaultType = defaultType;
		this.link = link;
	}
	
	public ChapterType getDefaultChapterType() {
		return defaultType;
	}
	
	public String getAuthor() {
		return author;
	}
	
	@Override
	public ChapterList createChapter(String title, int page) {
		Chapter child = new Chapter(this, null, title, page);
		addChapter(child);
		return child;
	}
	
	@Override
	public ChapterList createChapter(String title, int number, int page) {
		Chapter child = new Chapter(this, null, title, number, page);
		addChapter(child);
		return child;
	}

	@Override
	public String toString() {
		return "<html>[" + getTitle() + "]</html>";
	}

	@Override
	public String getHighlightTitle(String highlight, boolean letterCase) {
		StringBuilder
			builder = new StringBuilder(toString());
		
		int find = 0;
		while ((find = (!letterCase ? builder.toString().toLowerCase() : builder.toString()).indexOf((letterCase ? highlight : highlight.toLowerCase()), find)) >= 0) {
			String in = builder.toString().substring(find, find+highlight.length());
			String out = "<b><font color='#5cff47'>" + in + "</font></b>";
			
			builder.replace(find, find+in.length(), out);
			find += out.length();
		}
		
		return builder.toString();
	}

	@Override
	public void setProperty(String key, Object value) {
		ManagerBookConfig.setProperty(getID().toString() + "." + key, value);
	}

	@Override
	public void addProperty(String key, Object value) {
		ManagerBookConfig.addProperty(getID().toString() + "." + key, value);
	}

	@Override
	public Object getProperty(String key, Object defaultValue) {
		return ManagerBookConfig.getProperty(getID().toString() + "." + key, defaultValue);
	}

	public URL getLink() {
		return link;
	}
	
	//Only Helion
	public String getServerID() {
		String link = this.link.toString();
		if (!link.startsWith("http://helion.pl")) 
			return null;
		String id = link.substring(link.length()-10, link.length()-4);
		return id;
	}
	
	//Only Helion
	public URL getImageURL() {
		String id = getServerID();
		if (id == null) 
			return null;
		
		try {
			return new URL("https://static01.helion.com.pl/global/okladki/326x466/" + id + ".jpg");
		} catch (MalformedURLException e) {
			return null;
		}
	}
	
	//Only Helion
	public File getImageCache() {
		return new File(FOLDER_IMAGE_CACHE, getServerID() + ".jpg");
	}
	
	//Only Helion
	public BufferedImage getImage() {
		if (cacheImage != null)
			return cacheImage;
		
		URL url = getImageURL();
		if (url == null)
			return null;
		try {
			File f = getImageCache();
			cacheImage = ImageIO.read(getImageURL().openStream());
			ImageIO.write(cacheImage, "JPEG", f);
			return cacheImage;
		} catch (IOException e) {
			return null;
		}
	}
	
	protected void setImage(BufferedImage img) {
		this.cacheImage = img;
	}
}
