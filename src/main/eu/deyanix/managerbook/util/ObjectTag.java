package eu.deyanix.managerbook.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ObjectTag<K, V> {
	public static <K> List<ObjectTag<K, Object>> convertList(List<K> list) {
		List<ObjectTag<K, Object>> outputList = new ArrayList<ObjectTag<K, Object>>();
		for (K k : list) {
			outputList.add(new ObjectTag<K, Object>(k));
		}
		return outputList;
	}
	
	private final K mainObject;
	private V mainTag;
	private final Map<String, Object> tags = new HashMap<String, Object>();
	
	public ObjectTag(K mainObject) {
		this.mainObject = mainObject;
	}
	
	public ObjectTag(K mainObject, Map<String, V> tags) {
		this.mainObject = mainObject;
		this.tags.putAll(tags);
	}
	
	public void setTag(V value) {
		this.mainTag = value;
	}
	
	public V getTag() {
		return mainTag;
	}
	
	public void setTag(String tag, V value) {
		tags.put(tag, value);
	}
	
	@SuppressWarnings("unchecked")
	public V getTag(String tag) {
		return (V) tags.get(tag);
	}
	
	public void setObjectTag(String tag, Object value) {
		tags.put(tag, value);
	}
	
	public Object getObjectTag(String tag) {
		return tags.get(tag);
	}
	
	public K getObject() {
		return mainObject;
	}
}
